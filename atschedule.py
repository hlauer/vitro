import thread,threading

class AtSched(threading._Timer):
  """Wait until time, then do something"""

  def __init__(s,timeint, f, args=[],kwargs={}):
	import time

	if timeint>2*24*60*60: timeint-=time.time()
	if timeint<0: timeint=0				#to late, do it now
	super(AtSched,s).__init__(timeint,f,args,kwargs)
	s.start()					#start now

class AtLocalSched(threading._Timer):
  """Wait until time, then do something"""

  def __init__(s,tim, f, args=[],kwargs={},nextday=False):
	import time,calendar

	tl=time.localtime()
	fulltt=[tl[0],tl[1],tl[2],None,0,0,0,0,-1]
	fulltt[3:3+len(tim)]=tim
	t=time.mktime(fulltt)
	t-=time.time()		# how many left
	if t<0:
	  if nextday:
		fulltt=[tl[0],tl[1],tl[2]+1,None,0,0,0,0,-1]
		fulltt[3:3+len(tim)]=tim
		t=time.mktime(fulltt)
		t-=time.time()		# how many left
	  else: t=0			#schedule immediately

	super(AtLocalSched,s).__init__(t,f,args,kwargs)
	s.start()					#start now

def nextday(tim=None):
  import time,calendar

  tl=time.localtime()
  fulltt=[tl[0],tl[1],tl[2],None,0,0,0,0,-1]
  fulltt[3:3+len(tim)]=tim
  t=time.mktime(fulltt)
  if t<time.time():
    fulltt=[tl[0],tl[1],tl[2]+1,None,0,0,0,0,-1]
    fulltt[3:3+len(tim)]=tim
  t=time.mktime(fulltt)
  return time.localtime(t).tm_wday
