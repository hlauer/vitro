#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (C) Hermann Lauer 2014-2017
# may be distributed under the GPL version 3 or later

"""NAME
        %(prog)s - transfer boiler control over the net
    
SYNOPSIS
        %(prog)s [--help]
        
DESCRIPTION
        Transfers local mmap and pipe commend to a remote end.
        
FILES  
        memmap.py:
          definition of structure of the shared mem area.
     
SEE ALSO
        vitro
        
DIAGNOSTICS
        none
        
BUGS    
        many
    
AUTHOR
        Hermann.Lauer@iwr.uni-heidelberg.de
        {version}
"""

version="$Id$"
import syslog

#Helper functions
def hexcode(msg):
    return " ".join([hex(x) for x in bytearray(msg)])

import threading
class Sending(threading.Thread):
  """loop to log"""

  def __init__(s,soc,conargs):
    import memmap,select
    super().__init__()
    s.daemon=True 
    s.switchoff=None
    s.shm=memmap.StructMap(memmap.Vitro,'../i2c/vitro.mmap')
    s.ipipe=open('../i2c/vitrofrom.pipe','r+b',buffering=0)
    s.pipe=open('../i2c/vitroto.pipe','w+b',buffering=0)
    s.soc=soc
    s.conargs=conargs
    s.soc.connect(s.conargs)

  def run(s):
    import time
    while(True):
      try:
        print("{}".format(s.ipipe.read(1)))
        s.soc.send(bytes(s.shm))
      except OSError as e1:
        try:
          s.soc.connect(s.conargs)
          print("reconnected at {}".format(time.asctime()))
        except OSError as e2:
          print("While {} got {}".format(e1,e2))

class Receiving(threading.Thread):
  """loop to log"""

  def __init__(s,soc):
    import memmap,select
    super().__init__()
    s.daemon=True 
    s.switchoff=None
    s.shm=memmap.StructMem(memmap.Vitro,'../i2c/vitro.mmap')
    s.ipipe=open('../i2c/vitrofrom.pipe','w+b',buffering=0)
    s.pipe=open('../i2c/vitroto.pipe','r+b',buffering=0)
    s.soc=soc

  def run(s):
      while True:
          conn, addr = s.soc.accept()
          print('Connected by', addr)
          while True:
              data = conn.recv(256)
              print(">%s<"%data)
              if data==b'': break
              s.shm[:]=data
              s.ipipe.write(b'\x00')

class dummysocket(object):
    def send(s,b):
        print(b)

#-------------------------------------------------------------------------------
import argparse
class MyHelpFormatter(argparse.RawTextHelpFormatter):
    # It is necessary to use the new string formatting syntax, otherwise
    # the %(prog) expansion in the parent function is not going to work
    def _format_text(self, text):
        if '{version}' in text:
            text = text.format(version=version)
        return super(MyHelpFormatter, self)._format_text(text)

def ManOptionParser():
	return argparse.ArgumentParser(description=__doc__, 
                                       formatter_class=MyHelpFormatter)

#-------------------------------------------------------------------------------
#    Main 
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import sys,socket

    parser = ManOptionParser()
    parser.add_argument('-d', dest='debug', type=int,
                        metavar='DEBUG', help='DEBUG LEVEL')

    parser.add_argument('-s', '--send', dest='send', action='store_true',
                        help="send (instead of receive")

    parser.add_argument('-p', '--port', dest='port', type=int, default=4332,
                        help="use port instead of 4332")

    parser.add_argument('host', nargs=1, help='ip address to listen on or send to')

    args = parser.parse_args()

    syslog.openlog("vitroremote", syslog.LOG_PERROR, syslog.LOG_LOCAL3)

    if args.debug: print(v.init())

    _,_,ips = socket.gethostbyaddr(args.host[0])
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    if args.send:
        l=Sending(soc,(ips[0],args.port))    #prepare Thread
        l.start()       #start it
    else:
        soc.bind((ips[0], args.port))
        soc.listen(1)
        l=Receiving(soc)    #prepare Thread
        l.start()       #start it
