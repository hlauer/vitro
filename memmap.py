# This file is not executable, as seldom needed
#
# (C) Hermann Lauer 2002-2015
# may be distributed under the GPL version 3 or later

from ctypes import Structure, c_int, c_uint, c_float
class Vitro(Structure):
  ints=['LEIST','STARTS','MODE','BART','BETRS']
  floats=['KSOLL','KTS','ATS','AGTS','FLOW']
  fields=ints+floats
  _fields_ = [('nextsched', c_uint),('LEISTsum', c_uint)] + \
                [(n,c_int) for n in ints] + \
      		[(n,c_float) for n in floats]
  sets={b'\x00': fields,
        b's':('LEIST','FLOW')}
  try:
   del n
  except NameError: pass


def StructMap(struct,fn="memtest.mmap",create=False):
    """return a structure object on a mmap"""
    import mmap, ctypes

    mapopt=mmap.PROT_READ|mmap.PROT_EXEC|mmap.PROT_WRITE
    filemode="r+b"		#open readwrite
    try:
      fp=open(fn,filemode)
    except IOError as cause:
#      print "ioerror"
      if create:
        fp=open(fn,"w")
        fp.close()
        fp=open(fn,filemode)
      else: raise cause

    fp.seek(0,2)
    size=ctypes.sizeof(struct)	#determine size for mapping
    if size>fp.tell() and create:
      fp.seek(0,0)
      fp.write(b'\0'*size)	#initialize
    fp.seek(0,0)
    fno=fp.fileno()

    mmap=mmap.mmap(fno,size,mmap.MAP_SHARED,mapopt)	#memory map
    return struct.from_buffer(mmap)

def StructMem(struct,fn="memtest.mmap",create=False):
    """return a structure object on a mmap"""
    import mmap, ctypes

    mapopt=mmap.PROT_READ|mmap.PROT_EXEC|mmap.PROT_WRITE
    filemode="r+b"		#open readwrite
    try:
      fp=open(fn,filemode)
    except IOError as cause:
#      print "ioerror"
      if create:
        fp=open(fn,"w")
        fp.close()
        fp=open(fn,filemode)
      else: raise cause

    fp.seek(0,2)
    size=ctypes.sizeof(struct)	#determine size for mapping
    if size>fp.tell() and create:
      fp.seek(0,0)
      fp.write(b'\0'*size)	#initialize
    fp.seek(0,0)
    fno=fp.fileno()

    return mmap.mmap(fno,size,mmap.MAP_SHARED,mapopt)	#memory map
