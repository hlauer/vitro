#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (C) Hermann Lauer 2014-2015
# may be distributed under the GPL version 3 or later

"""NAME
        %(prog)s - communicate with boiler control
    
SYNOPSIS
        %(prog)s [--help]
        
DESCRIPTION
        Uses a serial line to communicate with a vessel control and fetch
        values from there. Those are stored in a shared memory area.
        
FILES  
        Datapoints.csv:
          Spreadsheed definitions of values, there size and kind. Part
          of the sheed of vserver on the net. Another controller could
          be selected with setting the column index to another value
        daten.log:
          showing the parsed definitions, which are use in the program
        memmap.py:
          definition of structure of the shared mem area.
          
     
SEE ALSO
        nothing
        
DIAGNOSTICS
        none
        
BUGS    
        many
    
AUTHOR
        Hermann.Lauer@iwr.uni-heidelberg.de
        {version}
"""

version="$Id$"
import syslog

class ViTrans(dict):
    """a hash to translate things"""

    def __init__(s,fn):
        """read csv file"""
        import csv
        idx=3                 #starting column of our boiler

        uparno=0
        global csvf
        csvf=csv.reader(open(fn))
        head=next(csvf)
        print(head)
#        print csvf.dialect, csvf.fieldnames
        for ln in csvf:
          try:
              descr,par=ln[0],ln[1]

              if not ln[idx]:   #test addr field to find other usage of the line
                  if ln[0] and not "".join(ln[1:]):
                      print("-> {}".format(descr))
                  continue
              addr,div,byte=int(ln[idx],0),ln[idx+1],int(ln[idx+2],0)

              mul=div.replace('\n'," ")         #extract divisor as multiplicator
              if div.startswith("/ "):
                  mul=1.0/int(div[2:])          #float
              if div=='': mul=1                 #int

              if not par:
                  par="Par{}".format(uparno)
                  uparno+=1
              print("{}\t{:X}\t{}\t{}\t{}".format(par,addr,byte,mul,descr))

              if par in s:
                print("DUPLICATE key, using {}".format(s[par]))
                continue
              s[par]=(addr,byte,mul,descr)
              
          except ValueError as cause:
              print("IGNORED {}".format(ln))
              continue

import serial
class Serial(serial.Serial):
  """encapsulate Serial communication"""

  def __init__(s,port='/dev/ttyS2'):
    """ initialise serial connection"""
  
    super().__init__()  
    s.conn = serial.Serial(port,
                     baudrate=4800,     #50, slowest for testing
                     bytesize=serial.EIGHTBITS,
                     parity=serial.PARITY_EVEN,
                     stopbits=serial.STOPBITS_TWO,
                     timeout=2,
                     xonxoff=0,
                     rtscts=0
                     )
#   getstat(conn)
    s.conn.setDTR(False)          # Start talking - setup for simple connector with vessel needing Power
    print(len(s.conn.read()))      #read initial

  def __call__(s):
    return s.conn  

  def getstat(c):
    res=c.getCTS(),c.getDSR(),c.getRI()    #,getCD()
    print(res)

#Helper functions
def hexcode(msg):
    return " ".join([hex(x) for x in bytearray(msg)])

def decodeBCD(bn):
    """calc int form BCD int"""
    h,l=bn>>4,bn&0xf
    if h>9 or l>9: return
    return h*10+l

class vtime(object):
  """maintain boiler time"""

  def __init__(s,v):
    s.v=v

  def read(s):
    import datetime
    ts=v.readpars('TIME')
    t=s.decode(ts)
    dt=t-datetime.datetime.today()
    print(dt,dt.total_seconds())

#ts='\x20\x14\x10\x07\x01\x08\x14\x23'
  def decode(s,ts):
    """decode a bcd timestring into a datetime object"""  
    import datetime

    yc,y,mo,d,dw,h,m,s=list(map(decodeBCD,ts))
    y=yc*100+y
    t=datetime.datetime(y,mo,d,h,m,s)
    print(t)
    return t

  def write(s):
    """write current time"""
    import datetime

    n=datetime.datetime.now().strftime(r"\x20\x%y\x%m\x%d\x0%w\x%H\x%M\x%S")
    ev="b\'{}\'".format(n)
    print(ev,n)
#    return eval(ev)
    res=s.v.writedata(0x88e,eval(ev))
    if args.debug: print(hexcode(res))

  def readerr(s,idx=0):
    """as a errent is only one byte code + timestamp, we do it in this class"""
  
    import datetime
    ts=v.readpars('FSpei',offset=idx*9)
    t=s.decode(ts[1:])
    print("{}: Code {:02X} at {}".format(idx,ts[0],t))

class VitroCom(object):
  """low level communication with vitronic"""

  def __init__(s,con):
    s.con=con  

  def init(s):
   """inititalize communication to vitronics"""

   s.con.flushInput()		#clean out old garbage
   for i in range(10):
    s.con.write(b'\x04')		#stop to be shure
    r=s.con.read()
    if len(r)>10: print("WARN: read {} bytes: {}".format(len(r),r))
    if not r:
        print("timeout %i"%i)
        continue        #wait longer
    if not r.endswith(b'\x05'):
      print("got {}".format(hexcode(r)))
      continue
#        print "got ",hex(ord(r))
    s.con.write(b'\x16\x00\x00')
    r=s.con.read()
    if not r:
        print("ack timeout %i"%i)
        continue

    return r=='\x06'

  def sendmsg(s,msg):
   """send a telegram"""
   from functools import reduce
   from struct import pack

   num=bytearray(msg)
   le=len(num)
   num.insert(0,le)        #len
   chksum=reduce(lambda x,y:x+y, num)%0x100
   m=pack(">BB",0x41,le)+msg+pack(">B",chksum)
   if args.debug: print("> {}".format(hexcode(m)))
   s.con.write(m)

  def readres(s):
    """read a telegram"""
    from functools import reduce
    from struct import unpack

    r=s.con.read(3)		#min expected
    head,reslen=unpack(">HB",r)
    if head != 0x641:
      print("unexpected {0}".format(hexcode(r)))
      return
    r=bytearray(s.con.read(reslen+1))	#read telegram with checksum
    if args.debug: print("< {}".format(hexcode(r)))

    num=r[:-1]			#cut chksum
    num.insert(0,reslen)
    chksum=reduce(lambda x,y:x+y, num)%0x100
    if r[-1] != chksum:
       print("chksum must be {:x}".format(chksum))
       return
    if r[0] != 1:
       print("unexpected message type: {:#x}".format(r[0]))
       return
    rlen=r[4]	#value code
    if len(r)-6!=rlen: print("Warning: len {}, but msg has {} result bytes".format(rlen,len(r)-6))
    return r[5:5+rlen]	#cut out

  def readdata(s,addr,bytes):
    """req an addr, receive bytes"""
    from struct import pack

    s.sendmsg(pack(">HHB",1,addr,bytes))
    return s.readres()

  def writedata(s,addr,data):
    """req an addr, receive bytes"""
    from struct import pack

    s.sendmsg(pack(">HHB",2,addr,len(data))+data)
    return s.readres()

#v.writedata(0x88e,'\x20\x14\x10\x04\x01\x00\x03\x00')

  def readpars(s,par,offset=0):
    """ read a table parameter as raw string. Length is determined by table"""

    addr,byte,mul,descr=vitr[par]
    print("{0}\t{1:#06x} {2} {3} {4}".format(par,addr+offset,byte,mul,descr))
    return s.readdata(addr+offset,byte)

  def readpar(s,par):
    """ read a table parameter as scaled int"""

    addr,byte,mul,descr=vitr[par]
    res=s.readdata(addr,byte)

    val=0
    shift=0
    for c in res:
      val|=c<<shift
      shift+=8
    if type(mul)==type(1.0) and byte==2:
      if val>1<<15: val-=1<<16
    if type(mul)==type(1) or type(mul)==type(1.0): val*=mul
    print("{0}\t{1}\t{2:#06x} {3} {4} {5}".format(par,val,addr,byte,mul,descr))
    return val

  __getattr__=readpar

  def writepar(s,par,value):
    """ write an iteger table parameter"""

    addr,byte,mul,descr=vitr[par]

    data=bytearray()
    for i in range(byte):
        data.append(value & 0xff)
        value>>=8
    if value:
        print('Overflow error: {}'.format(value))
        return
    return s.writedata(addr,data)

  def MODEnorm(s):
    return v.writepar('MODE',1)	#drinkwaterheating only
  def MODEon(s):
    return v.writepar('MODE',4)	#permanentely on

import threading
class Logging(threading.Thread):
  """loop to log"""

  def __init__(s,v):
    import memmap,select
    super(Logging, s).__init__()
    s.daemon=True 
    s.v=v
    s.switchoff=None
    s.period=30
    s.shm=memmap.StructMap(memmap.Vitro,'../i2c/vitro.mmap')
    s.pipe=open('../i2c/vitrofrom.pipe','w+b',buffering=0)
    s.ipipe=open('../i2c/vitroto.pipe','r+b',buffering=0)
    s.poll=select.poll()
    s.poll.register(s.ipipe,select.POLLIN)    #fileno

  def waitexec(s,rettime):
    import time,select,syslog

    while True:
      delay=rettime-time.time()
      if delay<0: return
      print("{} vitro loop: {}".format(time.asctime(),delay))
      ret=s.poll.poll(delay*1000)        #sleep
      if not ret: return
      cmd=s.ipipe.read(1)
      print("cmd ({}) {:X}".format(cmd,ord(cmd)))
      if cmd==b'0': s.v.writepar('MODE',0)#off
      if cmd==b'1': s.v.MODEnorm()        #drinkwaterheating only
      if cmd==b'4': s.v.MODEon()          #permanentely on
 
  def readvals(s):
    """generator/coroutine to read all/some of the values"""
    import time,select,syslog,struct

    STARTS=-1                           #take last STARTS as base for output, as it's update is often slow
    fields=(); idx=None
    while True:
      oldLEIST=s.shm.LEIST
      try:
        cnt=0
        for p in fields:
          val=s.v.readpar(p)
          setattr(s.shm,p,val)                    #store value
          cnt+=1
      except (TypeError,struct.error) as cause:
        print("Read failed: {},trying reinit".format(cause))
        s.v.init()
      try:	
        s.pipe.write(idx)
      except Exception as cause:
        print("write to pipe failed with {}".format(cause))
      s.shm.LEISTsum+=s.shm.LEIST
      if s.shm.LEIST and not oldLEIST:
        syslog.syslog(syslog.LOG_INFO,"Start #{}, {}h with {}%, total {}".format(STARTS+1,s.shm.BETRS/3600.0,s.shm.LEIST, s.shm.LEISTsum))
      idx=yield cnt
      STARTS=s.shm.STARTS
      fields=s.shm.sets[idx]

  def run(s):
    import time
    starttime=0

    readvals=s.readvals()
    readvals.send(None)             #init generator

    while True:
      cyclet=time.time()
      cnt=readvals.send(b'\x00')    #read all vals - 0 if switched off

      if s.switchoff and time.time()>s.switchoff:
        s.switchoff=None
        s.v.MODEnorm()	#switch off

      if cnt:  
          s.waitexec(starttime+10)
          readvals.send(b's')    #read special vals
          s.waitexec(starttime+20)
          readvals.send(b's')    #read special vals

      starttime+=s.period
      if starttime<cyclet: 	                #we missed to much, jump to real time
        syslog.syslog(syslog.LOG_WARNING,"vitro loop to slow by {0}s, jumped.".format(cyclet-starttime))
        starttime=(int(cyclet)/s.period+1)*s.period+1   #delay by one second
      s.waitexec(starttime)

  def offdelay(s,delay):
    import time

    s.switchoff=time.time()+delay
    return s.switchoff

#-------------------------------------------------------------------------------
import argparse
class MyHelpFormatter(argparse.RawTextHelpFormatter):
    # It is necessary to use the new string formatting syntax, otherwise
    # the %(prog) expansion in the parent function is not going to work
    def _format_text(self, text):
        if '{version}' in text:
            text = text.format(version=version)
        return super(MyHelpFormatter, self)._format_text(text)

def ManOptionParser():
	return argparse.ArgumentParser(description=__doc__, 
                                       formatter_class=MyHelpFormatter)

#-------------------------------------------------------------------------------
#    Main 
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import sys

    parser = ManOptionParser()
    parser.add_argument('-d', dest='debug', type=int,
                        metavar='DEBUG', help='DEBUG LEVEL')

#    parser.add_argument('files', nargs=2, help='input and output file names')

    args = parser.parse_args()

    vitr=ViTrans('Datapoints.csv')

    syslog.openlog("vitro", syslog.LOG_PERROR, syslog.LOG_LOCAL3)

    serialdev='/dev/ttyS0'
    try:
      if sys.implementation._multiarch.startswith('arm'):
        serialdev='/dev/ttyS2'
    except: pass

    ser=Serial(serialdev)
    v=VitroCom(ser())

    if args.debug: print(v.init())

    l=Logging(v)    #prepare Thread
    l.start()       #start it

    vt=vtime(v)	#time keeping object

#    from schedule import AtLocalSched
